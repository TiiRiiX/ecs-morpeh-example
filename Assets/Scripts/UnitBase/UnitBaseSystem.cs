using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using UnityEngine.AI;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
[CreateAssetMenu(menuName = "ECS/Systems/" + nameof(UnitBaseSystem))]
public class UnitBaseSystem : UpdateSystem
{
    private Filter filter;
    private Event<UnitSpawnEvent> unitSpawnEvent;
    private Filter unitBaseSpawnFilter;
    private Filter stateFilter;
    
    public override void OnAwake()
    {
        filter = World.Filter.With<UnitBaseComponent>().With<TransformComponent>().With<TeamTag>();
        unitSpawnEvent = World.GetEvent<UnitSpawnEvent>();
        unitBaseSpawnFilter = World.Filter.With<UnitBaseSpawnComponent>();
        stateFilter = World.Filter.With<GameStateComponent>();

        foreach (var entity in unitBaseSpawnFilter)
        {
            ref var unitBaseSpawn = ref entity.GetComponent<UnitBaseSpawnComponent>();
            var pointCount = unitBaseSpawn.PointsContainer.childCount;
            for (var i = 0; i < pointCount; i++)
            {
                var point = unitBaseSpawn.PointsContainer.GetChild(i);
                var newUnitBase = Instantiate(unitBaseSpawn.UnitBasePrefab, point.position, Quaternion.identity);
                var teamTagProvider = newUnitBase.GetComponent<TeamTagProvider>();
                var newUnitBaseEntity = teamTagProvider.Entity;
                ref var teamTag = ref newUnitBaseEntity.GetComponent<TeamTag>();
                teamTag.TeamId = i;
                ref var newUnitBaseComponent = ref newUnitBaseEntity.GetComponent<UnitBaseComponent>();
                newUnitBaseComponent.Renderer.material.color =
                    new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
                newUnitBaseComponent.TeamIdText.SetText($"{teamTag.TeamId}");
                ref var healthComponent = ref newUnitBaseEntity.GetComponent<HealthComponent>();
                healthComponent.CurrentHealth = healthComponent.StartHealth;
            }
        }
    }

    public override void OnUpdate(float deltaTime)
    {
        ref var stateComponent = ref stateFilter.FirstOrDefault().GetComponent<GameStateComponent>();
        if (stateComponent.GameState != GameStateComponent.State.Game) return;
        foreach (var entity in filter)
        {
            ref var unitBase = ref entity.GetComponent<UnitBaseComponent>();
            ref var teamTag = ref entity.GetComponent<TeamTag>();
            ref var transform = ref entity.GetComponent<TransformComponent>();
            if (unitBase.LastSpawnTime + unitBase.SpawnDelay < Time.time)
            {
                unitBase.LastSpawnTime = Time.time;
                var newUnit = Instantiate(unitBase.UnitPrefab, transform.Transform.position, Quaternion.identity);
                var teamTagProvider = newUnit.GetComponent<TeamTagProvider>();
                var newUnitEntity = teamTagProvider.Entity;
                newUnitEntity.GetComponent<TeamTag>().TeamId = teamTag.TeamId;
                ref var healthComponent = ref newUnitEntity.GetComponent<HealthComponent>();
                healthComponent.CurrentHealth = healthComponent.StartHealth;
                ref var unit = ref newUnitEntity.GetComponent<UnitComponent>();
                unit.Renderer.material.color = unitBase.Renderer.material.color;
                unitSpawnEvent.NextFrame(new UnitSpawnEvent { TeamId = teamTag.TeamId });
            }
        }
    }
}