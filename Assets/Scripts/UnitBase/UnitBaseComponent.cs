using System;
using Scellecs.Morpeh;
using TMPro;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
[Serializable]
public struct UnitBaseComponent : IComponent
{
    public GameObject UnitPrefab;
    public float SpawnDelay;
    [HideInInspector] public float LastSpawnTime;
    public Renderer Renderer;
    public TMP_Text TeamIdText;
}