using System.Linq;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using Unity.Profiling;
using UnityEngine;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
[CreateAssetMenu(menuName = "ECS/Systems/" + nameof(UnitSystem))]
public class UnitSystem : UpdateSystem
{
    private Filter unitFilter;
    private Filter unitBaseFilter;
    private Filter enemyFilter;
    private Stash<MovableComponent> movableStash;
    private Stash<TeamTag> teamTagStash;

    public override void OnAwake()
    {
        unitFilter = World.Filter.With<UnitComponent>().With<MovableComponent>().With<TeamTag>().With<AttackComponent>();
        unitBaseFilter = World.Filter.With<UnitBaseComponent>().With<TransformComponent>().With<TeamTag>();
        enemyFilter = World.Filter.With<MovableComponent>().With<TeamTag>();
        movableStash = World.GetStash<MovableComponent>();
        teamTagStash = World.GetStash<TeamTag>();
    }

    public override void OnUpdate(float deltaTime)
    {
        foreach (var entity in unitFilter)
        {
            ref var unit = ref entity.GetComponent<UnitComponent>();
            ref var movable = ref movableStash.Get(entity);
            ref var teamTag = ref teamTagStash.Get(entity);
            var tag = teamTag;

            if (unit.Target.IsNullOrDisposed())
            {
                var position = movable.Agent.transform.position;
                var visionRadius = unit.VisionRadius;
                var teamId = teamTag.TeamId;
                var newTarget = enemyFilter.Where(enemy => teamTagStash.Get(enemy).TeamId != teamId)
                    .OrderBy(_ => Random.Range(0f, 1f))
                    .FirstOrDefault(enemy =>
                        Vector3.Distance(movableStash.Get(enemy).Agent.transform.position,
                            position) < visionRadius);
                if (newTarget != null)
                {
                    unit.Target = newTarget;
                }
            }

            if (unit.Target.IsNullOrDisposed())
            {
                if (unit.EnemyBase.IsNullOrDisposed())
                {
                    var enemyBase = unitBaseFilter.Where(unitBase => teamTagStash.Get(unitBase).TeamId != tag.TeamId)
                        .OrderBy(_ => Random.Range(0f, 1f)).FirstOrDefault();
                    unit.EnemyBase = enemyBase;
                }

                if (!unit.EnemyBase.IsNullOrDisposed())
                {
                    movable.Agent.destination = unit.EnemyBase.GetComponent<TransformComponent>().Transform.position;    
                }
            }
            else
            {
                ref var targetMovable = ref movableStash.Get(unit.Target);
                movable.Agent.destination = targetMovable.Agent.transform.position;
            }
            
        }
    }
}