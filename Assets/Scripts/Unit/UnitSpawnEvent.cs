﻿using System;
using Scellecs.Morpeh;

[Serializable]
public struct UnitSpawnEvent : IEventData
{
    public int TeamId;
}