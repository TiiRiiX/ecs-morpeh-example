using System;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
[Serializable]
public struct UnitComponent : IComponent
{
    [HideInInspector] public Entity Target;
    public float VisionRadius;
    public Renderer Renderer;
    [HideInInspector] public Entity EnemyBase;
}