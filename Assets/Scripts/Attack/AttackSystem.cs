using System.Linq;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using Unity.Profiling;
using UnityEngine;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
[CreateAssetMenu(menuName = "ECS/Systems/" + nameof(AttackSystem))]
public class AttackSystem : UpdateSystem
{
    private Filter filter;
    private Filter blockFilter;
    private Filter enemyFilter;
    private Stash<TransformComponent> transformStash;
    private Stash<TeamTag> teamTagStash;
    
    public override void OnAwake()
    {
        filter = World.Filter.With<AttackComponent>().With<TransformComponent>().With<TeamTag>().Without<AttackBlockComponent>();
        enemyFilter = World.Filter.With<HealthComponent>().With<TransformComponent>().With<TeamTag>();
        blockFilter = World.Filter.With<AttackBlockComponent>();
        transformStash = World.GetStash<TransformComponent>();
        teamTagStash = World.GetStash<TeamTag>();
    }

    public override void OnUpdate(float deltaTime)
    {
        foreach (var entity in blockFilter)
        {
            ref var block = ref entity.GetComponent<AttackBlockComponent>();
            block.Duration -= Time.deltaTime;
            if (block.Duration <= 0)
            {
                entity.RemoveComponent<AttackBlockComponent>();
            }
        }

        foreach (var entity in filter)
        {
            ref var transformComponent = ref transformStash.Get(entity);
            ref var attack = ref entity.GetComponent<AttackComponent>();
            ref var teamTag = ref teamTagStash.Get(entity); 
            var position = transformComponent.Transform.position;
            var attackRadius = attack.AttackRadius;
            var teamId = teamTag.TeamId;
            var target = enemyFilter.Where(enemy => teamTagStash.Get(enemy).TeamId != teamId)
                .FirstOrDefault(enemy =>
                    Vector3.Distance(transformStash.Get(enemy).Transform.position,
                        position) < attackRadius);
            if (target != null)
            {
                if (target.Has<DamageRequest>())
                {
                    ref var damageRequest = ref target.GetComponent<DamageRequest>();
                    damageRequest.Damage += attack.Damage;
                }
                else
                {
                    ref var damageRequest = ref target.AddComponent<DamageRequest>();
                    damageRequest.Damage = attack.Damage;
                }
                
                ref var blockComponent = ref entity.AddComponent<AttackBlockComponent>();
                blockComponent.Duration = attack.AttackDelay;
            }
        }
    }
}
