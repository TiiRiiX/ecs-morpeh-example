﻿using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using UnityEngine.SceneManagement;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
[CreateAssetMenu(menuName = "ECS/Systems/" + nameof(GameUISystem))]
public class GameUISystem : UpdateSystem
{
    private Event<GameStateRequest> stateRequest;
    private Event<GameStateEvent> stateEvent;
    private Filter stateFilter;
    private Filter stateContainerFilter;
    
    public override void OnAwake()
    {
        stateRequest = World.GetEvent<GameStateRequest>();
        stateEvent = World.GetEvent<GameStateEvent>();
        stateFilter = World.Filter.With<GameStateComponent>();
        stateContainerFilter = World.Filter.With<StateUIContainerComponent>();
        var currentState = stateFilter.FirstOrDefault();
        ref var gameState = ref currentState.GetComponent<GameStateComponent>();
        foreach (var entity in stateContainerFilter)
        {
            ref var stateUIContainer = ref entity.GetComponent<StateUIContainerComponent>();
            RefreshContainer(stateUIContainer, gameState.GameState);
        }
    }

    private void RefreshContainer(StateUIContainerComponent containerComponent, GameStateComponent.State state)
    {
        if (containerComponent.ToggleOnState == state)
        {
            if (!containerComponent.Container.gameObject.activeSelf)
            {
                containerComponent.Container.gameObject.SetActive(true);    
            }
        }
        else
        {
            if (containerComponent.Container.gameObject.activeSelf)
            {
                containerComponent.Container.gameObject.SetActive(false);
            }
        }
    }

    public override void OnUpdate(float deltaTime)
    {
        foreach (var eventBatched in stateEvent.BatchedChanges)
        {
            foreach (var entity in stateContainerFilter)
            {
                ref var container = ref entity.GetComponent<StateUIContainerComponent>();
                RefreshContainer(container, eventBatched.State);
            }
        }
    }

    public void StartLevel()
    {
        stateRequest.NextFrame(new GameStateRequest { State = GameStateComponent.State.Game });
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}