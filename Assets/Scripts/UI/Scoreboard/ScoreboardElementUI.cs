﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScoreboardElementUI : MonoBehaviour
{
    [SerializeField] private TMP_Text teamText;
    [SerializeField] private TMP_Text unitCountText;
    [SerializeField] private Image icon;

    public void FIll(string teamName, int unitCount, Color color)
    {
        teamText.SetText(teamName);
        unitCountText.SetText($"{unitCount}");
        icon.color = color;
    }
}