﻿using System;
using System.Collections.Generic;
using System.Linq;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
[CreateAssetMenu(menuName = "ECS/Systems/" + nameof(ScoreboardSystem))]
public class ScoreboardSystem : UpdateSystem
{
    private Event<KillEvent> killEvent;
    private Event<UnitSpawnEvent> spawnEvent;
    private Filter unitFilter;
    private Filter filter;
    private Filter unitBaseFilter;
    private Filter unitsCounterFilter;

    public class ScoreboardElementData : IComparable
    {
        public int Count;
        public Color Color;
        
        public int CompareTo(object obj)
        {
            return Count.CompareTo(((ScoreboardElementData)obj).Count);
        }
    }
    
    public override void OnAwake()
    {
        killEvent = World.GetEvent<KillEvent>();
        unitsCounterFilter = World.Filter.With<UnitsCounterComponent>();
        spawnEvent = World.GetEvent<UnitSpawnEvent>();
        filter = World.Filter.With<ScoreboardComponent>();
        unitFilter = World.Filter.With<UnitComponent>().With<TeamTag>();
        unitBaseFilter = World.Filter.With<UnitBaseComponent>().With<TeamTag>();

        foreach (var scoreboardEntity in filter)
        {
            RefreshScoreboard(scoreboardEntity);
        }
        RefreshUnitsCounter();
    }

    private void RefreshScoreboard(Entity scoreboardEntity)
    {
        ref var scoreboard = ref scoreboardEntity.GetComponent<ScoreboardComponent>();
        foreach (Transform child in scoreboard.Container)
        {
            Destroy(child.gameObject);
        }

        var unitCount = new Dictionary<TeamTag, ScoreboardElementData>();
        foreach (var baseEntity in unitBaseFilter)
        {
            ref var teamTag = ref baseEntity.GetComponent<TeamTag>();
            ref var baseUnit = ref baseEntity.GetComponent<UnitBaseComponent>();
            var teamId = teamTag.TeamId;
            unitCount.Add(teamTag, new ScoreboardElementData
            {
                Count = unitFilter.Count(unit =>
                    unit.GetComponent<TeamTag>().TeamId == teamId),
                Color = baseUnit.Renderer.material.color
            });
        }
        
        foreach (var unitsPair in unitCount.OrderByDescending(pair => pair.Value))
        {
            var newElement = Instantiate(scoreboard.ElementPrefab, scoreboard.Container);
            newElement.FIll($"Team {unitsPair.Key.TeamId}", unitsPair.Value.Count, unitsPair.Value.Color);
        }
    }

    public override void OnUpdate(float deltaTime)
    {
        var isRefreshed = false;
        foreach (var change in killEvent.BatchedChanges)
        {
            if (change.IsUnitBase)
            {
                Debug.Log($"Unit Base {change.TeamId}({change.EntityId}) destroyed");    
            }

            if (isRefreshed) break;
            isRefreshed = true;
            foreach (var scoreboardEntity in filter)
            {
                RefreshScoreboard(scoreboardEntity);
            }

            RefreshUnitsCounter();
        }

        foreach (var change in spawnEvent.BatchedChanges)
        {
            if (isRefreshed) break;
            isRefreshed = true;
            foreach (var scoreboardEntity in filter)
            {
                RefreshScoreboard(scoreboardEntity);
            }
            
            RefreshUnitsCounter();
        }
    }

    private void RefreshUnitsCounter()
    {
        foreach (var counterEntity in unitsCounterFilter)
        {
            ref var counter = ref counterEntity.GetComponent<UnitsCounterComponent>();
            counter.Text.SetText($"Units: {unitFilter.GetLengthSlow()}");
        }
    }
}