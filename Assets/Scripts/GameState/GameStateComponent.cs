﻿using System;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
[Serializable]
public struct GameStateComponent : IComponent
{
    [Serializable]
    public enum State
    {
        Start,
        Game,
        Finish
    }

    public State GameState;
}
