﻿using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
[CreateAssetMenu(menuName = "ECS/Systems/" + nameof(GameStateSystem))]
public class GameStateSystem : UpdateSystem
{
    private Entity entity;
    private Event<GameStateRequest> stateRequestEvent;
    private Event<GameStateEvent> stateEvent;

    public override void OnAwake()
    {
        entity = World.CreateEntity();
        stateRequestEvent = World.GetEvent<GameStateRequest>();
        ref var state = ref entity.AddComponent<GameStateComponent>();
        state.GameState = GameStateComponent.State.Start;
        stateEvent = World.GetEvent<GameStateEvent>();
        stateEvent.NextFrame(new GameStateEvent { State = GameStateComponent.State.Start });
    }

    public override void OnUpdate(float deltaTime)
    {
        foreach (var request in stateRequestEvent.BatchedChanges)
        {
            ref var stateComponent = ref entity.GetComponent<GameStateComponent>();
            stateComponent.GameState = request.State;
            stateEvent.NextFrame(new GameStateEvent { State = request.State });
            Debug.Log($"Change game state: {request.State}");
        }
    }
}