﻿using System;
using Scellecs.Morpeh;

[Serializable]
public struct GameStateRequest : IEventData
{
    public GameStateComponent.State State;
}