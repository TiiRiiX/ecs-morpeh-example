﻿using System;
using Scellecs.Morpeh;

[Serializable]
public struct GameStateEvent : IEventData
{
    public GameStateComponent.State State;
}