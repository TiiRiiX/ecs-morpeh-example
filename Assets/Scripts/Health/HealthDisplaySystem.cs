using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
[CreateAssetMenu(menuName = "ECS/Systems/" + nameof(HealthDisplaySystem))]
public class HealthDisplaySystem : UpdateSystem
{
    private Filter filter;
    private Camera camera;
    
    public override void OnAwake()
    {
        filter = World.Filter.With<HealthComponent>().With<HealthDisplayComponent>();
        camera = Camera.main;
    }

    public override void OnUpdate(float deltaTime)
    {
        foreach (var entity in filter)
        {
            ref var hpDisplay = ref entity.GetComponent<HealthDisplayComponent>();
            ref var hpComponent = ref entity.GetComponent<HealthComponent>();
            hpDisplay.Canvas.transform.LookAt( hpDisplay.Canvas.transform.position + camera.transform.rotation * Vector3.back, 
                camera.transform.rotation * Vector3.down);
            var health = hpComponent.CurrentHealth / (float)hpComponent.StartHealth;
            if (health == 1 && hpDisplay.Bar.gameObject.activeSelf)
            {
                hpDisplay.Bar.gameObject.SetActive(false);
            }
            if (hpDisplay.Bar.value != health)
            {
                hpDisplay.Bar.value = health;
                hpDisplay.Bar.gameObject.SetActive(true);
            }
        }
    }
}