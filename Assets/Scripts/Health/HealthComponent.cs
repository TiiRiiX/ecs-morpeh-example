using System;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using Object = UnityEngine.Object;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
[Serializable]
public struct HealthComponent : IComponent, IDisposable
{
    public int StartHealth;
    public int CurrentHealth;
    public GameObject Value;
    
    public void Dispose()
    {
        Object.Destroy(Value);
    }
}