using System.Linq;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
[CreateAssetMenu(menuName = "ECS/Systems/" + nameof(HealthSystem))]
public class HealthSystem : UpdateSystem
{
    private Filter damagedFilter;
    private Stash<UnitBaseComponent> unitBaseStash;
    private Filter unitBaseFilter;
    private Filter unitsFilter;
    private Event<KillEvent> killEvent;
    private Event<GameStateRequest> stateEvent;

    public override void OnAwake()
    {
        killEvent = World.GetEvent<KillEvent>();
        unitBaseStash = World.GetStash<UnitBaseComponent>();
        unitsFilter = World.Filter.With<UnitComponent>().With<TeamTag>();
        unitBaseFilter = World.Filter.With<UnitBaseComponent>();
        stateEvent = World.GetEvent<GameStateRequest>();
        World.GetStash<HealthComponent>().AsDisposable();
        damagedFilter = World.Filter.With<HealthComponent>().With<DamageRequest>();
        foreach (var entity in World.Filter.With<HealthComponent>())
        {
            ref var hp = ref entity.GetComponent<HealthComponent>();
            hp.CurrentHealth = hp.StartHealth;
        }
    }

    public override void OnUpdate(float deltaTime)
    {
        foreach (var entity in damagedFilter)
        {
            ref var damaged = ref entity.GetComponent<DamageRequest>();
            ref var health = ref entity.GetComponent<HealthComponent>();
            health.CurrentHealth -= damaged.Damage;
            entity.RemoveComponent<DamageRequest>();
            if (health.CurrentHealth <= 0)
            {
                ref var teamTag = ref entity.GetComponent<TeamTag>();
                var teamId = teamTag.TeamId;
                var isUnitBase = false;
                if (unitBaseStash.Has(entity))
                {
                    foreach (var unit in unitsFilter.Where(unit => 
                                 unit.GetComponent<TeamTag>().TeamId == teamId))
                    {
                        unit.Dispose();
                    }

                    isUnitBase = true;
                }
                killEvent.NextFrame(new KillEvent { EntityId = entity.ID, TeamId = teamId, IsUnitBase = isUnitBase });
                entity.Dispose();
                if (isUnitBase && unitBaseFilter.GetLengthSlow() == 1)
                {
                    stateEvent.NextFrame(new GameStateRequest { State = GameStateComponent.State.Finish });
                }
            }
        }
    }
}