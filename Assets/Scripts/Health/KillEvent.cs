﻿using System;
using Scellecs.Morpeh;

[Serializable]
public struct KillEvent : IEventData
{
    public EntityId EntityId;
    public int TeamId;
    public bool IsUnitBase;
}