using System;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using UnityEngine.AI;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
[Serializable]
public struct MovableComponent : IComponent
{
    public NavMeshAgent Agent;
    public float Speed;
    public float TimeChangeDirection;
    [HideInInspector] public float LastTimeChangeDireciton;
}